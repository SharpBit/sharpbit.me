# My Website
Welcome to my website. This is a simple site for my personal use and experience. You could call it a "mini-project". I have included a bit of background info, projects I'm working on, and assets for my profile pictures, avatars, and more.<br />
<br />
If you would like to check it out, please click [here](http://www.sharpbit.me). If you feel I should add or fix something, please do so by opening an [issue](https://github.com/SharpBit/sharpbit.me/issues/new).
